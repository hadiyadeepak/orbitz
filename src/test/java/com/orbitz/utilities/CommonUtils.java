package com.orbitz.utilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CommonUtils {
	
	public static String getFutureDate(String addDayAsStringInTodayDate) {
		Date m = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(m);
		cal.add(Calendar.DATE, Integer.parseInt(addDayAsStringInTodayDate)); // 10 is the days you want to add or subtract
		m = cal.getTime();
		return new SimpleDateFormat("MM/dd/yyyy").format(m).toString();
	}

	
	
}
