package com.orbitz.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class FlightsPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "lbl.flighttype.flightspage")
	private QAFWebElement lblFlightType;

	@FindBy(locator = "tbox.flightorigin.flightspage")
	private QAFWebElement tboxFlightOrigin;

	@FindBy(locator = "tbox.flightdestination.flightspage")
	private QAFWebElement tboxFlightDestination;

	@FindBy(locator = "tbox.flightdepartingdate.flightspage")
	private QAFWebElement tboxFlightDepartingDate;

	@FindBy(locator = "btn.search.flightspage")
	private QAFWebElement btnSearch;

	@FindBy(locator = "btn.autosuggestionclose.flightspage")
	private QAFWebElement btnAutosuggesionListClose;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getLblFlightType() {
		return lblFlightType;
	}

	public QAFWebElement getBtnAutosuggesionListClose() {
		return btnAutosuggesionListClose;
	}

	public QAFWebElement getTboxFlightOrigin() {
		return tboxFlightOrigin;
	}

	public QAFWebElement getTboxFlightDestination() {
		return tboxFlightDestination;
	}

	public QAFWebElement getTboxFlightDepartingDate() {
		return tboxFlightDepartingDate;
	}

	public QAFWebElement getBtnSearch() {
		return btnSearch;
	}

	public void selectFlightType(String flightType) {
		QAFExtendedWebElement linkflightType = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("lbl.flighttype.flightspage"),
				"One way"));
		linkflightType.waitForVisible();
		linkflightType.click();
	}

	public void selectFlightOriginCity(String origin) {

		tboxFlightOrigin.sendKeys(origin);
		btnAutosuggesionListClose.waitForVisible();
		tboxFlightOrigin.sendKeys(Keys.ENTER);

	}

	public String getSourceCityCode() {
		return tboxFlightOrigin.getText().substring(
				tboxFlightOrigin.getText().indexOf("(") + 1,
				tboxFlightOrigin.getText().indexOf("(") + 4);
	}

	public void selectFlightDestCity(String dest) {

		tboxFlightDestination.sendKeys(dest);
		btnAutosuggesionListClose.waitForVisible();
		tboxFlightDestination.sendKeys(Keys.ENTER);
	}

	public String getDestCityCode() {
		return tboxFlightDestination.getText().substring(
				tboxFlightDestination.getText().indexOf("(") + 1,
				tboxFlightDestination.getText().indexOf("(") + 4);
	}

	public void selectDepartingDate(String date) {

		tboxFlightDepartingDate.sendKeys(date+Keys.ESCAPE);
	

	}

	public void clickOnSearchBtn() {

		btnSearch.click();
	}

}
