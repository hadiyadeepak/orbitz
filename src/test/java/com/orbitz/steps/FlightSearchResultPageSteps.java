package com.orbitz.steps;

import static com.qmetry.qaf.automation.step.CommonStep.verifyPresent;

import java.util.List;

import org.hamcrest.Matchers;

import com.orbitz.components.FlightsList;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;



public class FlightSearchResultPageSteps extends WebDriverBaseTestPage<WebDriverTestPage>{

	@FindBy(locator = "list.flightslist.flightsearchresultpage")
	private List<FlightsList> listFlightsList;
	
	@QAFTestStep(description="user should see the result of the searched flights")
	public void verifyFlightSearchResultPage()
	{
		verifyPresent("select.sortbyoptions.flightsearchresultpage");
	}
	
	@QAFTestStep(description="user verifies first two flights from {0} and {1} from the result page")
	public void verifySearchedFlights(String source,String dest)
	{
		int counter=0;
		for(int i=0; i<2;i++)
		{
			
			if(listFlightsList.get(i).getFlightSourceDest().getText().contains(source) && listFlightsList.get(i).getFlightSourceDest().getText().endsWith(dest))
				counter++;
		}
		Validator.verifyThat("First two flights are from "+source+" to "+dest,counter, Matchers.equalTo(2));
	}
	
	@QAFTestStep(description="user prints all the flight details in the Dashboard")
	public void printAllFlightDetails()
	{
		for(FlightsList flight:listFlightsList)
		{
			String sourceDest = flight.getFlightSourceDest().getText();
			String duration = flight.getFlightDuration().getText();
			String timing = flight.getFlightDepartureAndArrivalTime().getText();
			String price = flight.getFlightPrice().getText();
			String airline = flight.getFlightAirlineType().getText();

			Reporter.log("Flight detail ::: Arline> " + airline + " \nSource and Dest> "
					+ sourceDest + " \tDuration> " + duration + " \tTime>" + timing
					+ " \tPrice> " + price);
		}
		Reporter.log("Total flights =====> "+listFlightsList.size());
		
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}
	
	
}
