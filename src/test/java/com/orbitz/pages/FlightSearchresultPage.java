package com.orbitz.pages;

import java.util.List;

import org.hamcrest.Matchers;

import com.orbitz.beans.FlightsDetailBean;
import com.orbitz.components.FlightsList;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class FlightSearchresultPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "select.sortbyoptions.flightsearchresultpage")
	private QAFWebElement selectSortByOptions;

	@FindBy(locator = "list.flightslist.flightsearchresultpage")
	private List<FlightsList> listFlightsList;

	

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
		driver.get("/");
	}

	public QAFWebElement getSelectSortByOptions() {
		return selectSortByOptions;
	}

	public void waitForPageToLoad() {
		selectSortByOptions.waitForVisible();
	}

	public List<FlightsList> getListFlightsList() {
		return listFlightsList;
	}

	public void printFlightResultInDashboard() {
		for (FlightsList flight : listFlightsList) {
			String sourceDest = flight.getFlightSourceDest().getText();
			String duration = flight.getFlightDuration().getText();
			String timing = flight.getFlightDepartureAndArrivalTime().getText();
			String price = flight.getFlightPrice().getText();
			String airline = flight.getFlightAirlineType().getText();

			Reporter.log("Flight detail ::: Arline> " + airline + " \nSource and Dest> "
					+ sourceDest + " \tDuration> " + duration + " \tTime>" + timing
					+ " \tPrice> " + price);
		}
		Reporter.log("Total flights =====> "+listFlightsList.size());
	}

	public FlightsDetailBean selectFlightByIndex(int index) {
		FlightsList flight = listFlightsList.get(index);
		flight.getLnkAirlineRating().waitForVisible();
		String sourceDest = flight.getFlightSourceDest().getText();
		String duration = flight.getFlightDuration().getText();
		String timing = flight.getFlightDepartureAndArrivalTime().getText();
		String price = flight.getFlightPrice().getText();
		String airline = flight.getFlightAirlineType().getText();
		flight.getBtnSelectFlight().click();
		return new FlightsDetailBean(sourceDest, airline, timing, duration, price);
	}
	
	public void verifyFlightsBySourceDest(String source,String dest)
	{
		
		int counter=0;
		for(int i=0; i<2;i++)
		{
			if(listFlightsList.get(i).getFlightSourceDest().getText().contains(source) && listFlightsList.get(i).getFlightSourceDest().getText().endsWith(dest))
				counter++;
		}
		Validator.verifyThat("First two flights are from "+source+" to "+dest,counter, Matchers.equalTo(2));
	}
	
	
}
