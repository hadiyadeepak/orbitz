package com.orbitz.steps;

import static com.qmetry.qaf.automation.step.CommonStep.*;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

public class HomePageSteps extends WebDriverBaseTestPage<WebDriverTestPage> {

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
	}

	@QAFTestStep(description = "user opens orbitz application in the browser")
	public void openApplication() {
		openPage(null, null);
	}

	@QAFTestStep(description = "user opens {0} category page")
	public void openCategoryPage(String category) {
		QAFExtendedWebElement categoryLink = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("link.category.homepage"),
				category));
		categoryLink.waitForVisible();
		categoryLink.click();
	}

}
