package com.orbitz.tests;

import java.util.Map;

import org.testng.annotations.Test;

import com.orbitz.pages.FlightSearchresultPage;
import com.orbitz.pages.FlightsPage;
import com.orbitz.pages.HomePage;
import com.orbitz.utilities.CommonUtils;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestCase;

public class FlightSearchTest extends WebDriverTestCase{
	
	
	@QAFDataProvider(dataFile="resources/data/data.csv")
	@Test
	public void testFlightSearch(Map<String,String> data)
	{
	
		
		//open the application
		HomePage homepage=new HomePage();
		homepage.openApplication();
		
		//select flights from the category
		homepage.selectCategory("Flights");
		
		//select flight type
		FlightsPage flightsPage=new FlightsPage();
		flightsPage.selectFlightType("One way");
		
		//select origin city
		flightsPage.selectFlightOriginCity(data.get("source"));
		
		//select destination city
		flightsPage.selectFlightDestCity(data.get("dest"));
		
		//select departing date
		flightsPage.selectDepartingDate(CommonUtils.getFutureDate("3"));
		
		//click on flight search button
		flightsPage.clickOnSearchBtn();
		
		//waiting for the flights result page
		FlightSearchresultPage resultPage=new FlightSearchresultPage();
		resultPage.waitForPageToLoad();
		
		//verifying first two flights from the result
		resultPage.verifyFlightsBySourceDest(data.get("source")	, data.get("dest"));

		//printing the searched flight details in the Dashboard. 
		resultPage.printFlightResultInDashboard();
		
		
	}

}
