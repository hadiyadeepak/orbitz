package com.orbitz.beans;

public class FlightsDetailBean {

	String sourceDest;
	String duration;
	String timing;
	String price;
	String airline;

	public FlightsDetailBean(String sourceDest, String airline, String timing,
			String duration, String price) {
		this.airline = airline;
		this.duration = duration;
		this.price = price;
		this.sourceDest = sourceDest;
		this.timing = timing;
	}

	public String getSourceDest() {
		return sourceDest;
	}

	public void setSourceDest(String sourceDest) {
		this.sourceDest = sourceDest;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getTiming() {
		return timing;
	}

	public void setTiming(String timing) {
		this.timing = timing;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}

}
