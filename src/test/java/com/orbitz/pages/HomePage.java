package com.orbitz.pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class HomePage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "link.category.homepage")
	private QAFWebElement linkCategory;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
		driver.get("/");
		
	}

	public QAFWebElement getLinkCategoryHomepage() {
		return linkCategory;
	}
	
	public void openApplication()
	{
		openPage(null);
	}
	
	public void selectCategory(String categoryName)
	{
		QAFExtendedWebElement category=new QAFExtendedWebElement(String.format(pageProps.getString("link.category.homepage"), "Flights"));
		category.waitForVisible();
		category.click();
	}

}
