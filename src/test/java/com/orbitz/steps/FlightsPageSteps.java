package com.orbitz.steps;

import static com.qmetry.qaf.automation.step.CommonStep.click;
import static com.qmetry.qaf.automation.step.CommonStep.sendKeys;
import static com.qmetry.qaf.automation.step.CommonStep.waitForVisible;

import org.hamcrest.Matchers;
import org.openqa.selenium.Keys;

import com.orbitz.utilities.CommonUtils;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class FlightsPageSteps extends WebDriverBaseTestPage<WebDriverTestPage> {

	@QAFTestStep(description = "user should see the Flights booking Page")
	public void verifyTitleOfTheFlightBookingPage() {
		Validator.verifyThat(driver.getTitle(),
				Matchers.containsString("Book Cheap Flights & Airline Tickets"));
	}

	@QAFTestStep(description = "user selects {0} trip type from Flights page")
	public void selectFlightTripType(String flightTripType) {
		QAFExtendedWebElement tripType = new QAFExtendedWebElement(String.format(
				ConfigurationManager.getBundle().getString("lbl.flighttype.flightspage"),
				flightTripType));
		tripType.waitForVisible();
		tripType.click();
	}

	@QAFTestStep(description = "user enters {0} as source city")
	public void selectSourceCity(String source) {
		QAFWebElement sourceTB =
				new QAFExtendedWebElement("tbox.flightorigin.flightspage");
		sourceTB.sendKeys(source);
		waitForVisible("btn.autosuggestionclose.flightspage");
		sourceTB.sendKeys(Keys.ENTER);
	}

	@QAFTestStep(description = "user enters {0} as destination city")
	public void selectDestCity(String dest) {
		QAFWebElement destTB =
				new QAFExtendedWebElement("tbox.flightdestination.flightspage");
		destTB.sendKeys(dest);
		waitForVisible("btn.autosuggestionclose.flightspage");
		destTB.sendKeys(Keys.ENTER);
	}

	@QAFTestStep(description = "user selects departure date")
	public void selectDepartureDate() {
		QAFWebElement dateTb =
				new QAFExtendedWebElement("tbox.flightdepartingdate.flightspage");
		dateTb.sendKeys(CommonUtils.getFutureDate("3") + Keys.ESCAPE);

	}

	@QAFTestStep(description = "user clicks on search flights")
	public void clickOnSearchButton() {
		click("btn.search.flightspage");
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

}
