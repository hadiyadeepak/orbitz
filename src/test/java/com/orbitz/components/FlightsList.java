package com.orbitz.components;



import org.hamcrest.Matchers;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class FlightsList extends QAFWebComponent {

	public FlightsList(String locator) {
		super(locator);
		// TODO Auto-generated constructor stub
	}

	@FindBy(locator = "lbl.flightsourcedest.flightsearchresultpage")
	private QAFWebElement flightSourceDest;

	@FindBy(locator = "lbl.flightprice.flightsearchresultpage")
	private QAFWebElement flightPrice;

	@FindBy(locator = "lbl.flightdepartureandarrivaltime.flightsearchresultpage")
	private QAFWebElement flightDepartureAndArrivalTime;

	@FindBy(locator = "lbl.flightduration.flightsearchresultpage")
	private QAFWebElement flightDuration;

	@FindBy(locator = "lbl.airlinetype.flightsearchresultpage")
	private QAFWebElement flightAirlineType;
	
	@FindBy(locator = "btn.selectflight.flightsearchresultpage")
	private QAFWebElement btnSelectFlight;
	
	@FindBy(locator="lnk.airlinerating.flightsearchresultpage")
	private QAFWebElement lnkAirlineRating;

	public QAFWebElement getLnkAirlineRating() {
		return lnkAirlineRating;
	}

	public QAFWebElement getBtnSelectFlight() {
		return btnSelectFlight;
	}

	public QAFWebElement getFlightAirlineType() {
		return flightAirlineType;
	}

	public QAFWebElement getFlightDepartureAndArrivalTime() {
		return flightDepartureAndArrivalTime;
	}

	public QAFWebElement getFlightDuration() {
		return flightDuration;
	}

	public QAFWebElement getFlightSourceDest() {
		return flightSourceDest;
	}

	public QAFWebElement getFlightPrice() {
		return flightPrice;
	}

	public boolean equals(Object obj) {
		if (obj != null && obj instanceof String) {
			return flightSourceDest.getText().equalsIgnoreCase((String) obj);
		}

		return super.equals(obj);
	}

	public boolean compareTo(String source, String dest) {
		return Validator.verifyThat(
				flightSourceDest.getText().contains(source)
						&& flightSourceDest.getText().endsWith(dest),
				Matchers.equalTo(true));
	}

}
